function factorialize(num) {
  var i;
  for (i = 1; num > 0; num--) {
    i *= num;
  }
  return i;
}
var result = factorialize(5);
console.log (result);
